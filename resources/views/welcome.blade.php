<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                @vite('resources/css/app.css')
                <title>Laravel Tailwind Example</title>
            </head>
            <body>
                <div class="max-w-md  m-24 rounded overflow-hidden shadow-lg">
                    <img class="w-full" src="https://picsum.photos/400/300" alt="Blog Image">
                    <div class="px-6 py-4">
                        <h2 class="font-bold text-2xl mb-2">Hello World!</h2>
                        <p class="mt-3 text-gray-600 text-base">
                            This is a new Laravel Tailwind project deployed to Kinsta!
                        </p>
                        <button class="mt-4 bg-blue-500 text-white font-bold py-2 px-4 rounded">
                            Read More
                        </button>
                    </div>
                </div>
            </body>
        </html>

